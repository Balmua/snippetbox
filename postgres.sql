CREATE TABLE snippets
(
    id      serial primary key,
    title   VARCHAR(100) NOT NULL,
    content TEXT         NOT NULL,
    created timestamp    NOT NULL,
    expires timestamp    NOT NULL
);
-- Add an index on the created column.
CREATE INDEX idx_snippets_created ON snippets (created);

INSERT INTO snippets (title, content, created, expires)
VALUES ('An old silent pond',
        'An old silent pond...\nA frog jumps into the pond,\nsplash! Silence again.\n\n– Matsuo Bashō',
        now(),
        now());
INSERT INTO snippets (title, content, created, expires)
VALUES ('Over the wintry forest',
        'Over the wintry\nforest, winds howl in rage\nwith no leaves to blow.\n\n– Natsume Soseki',
        now(),
        now());
INSERT INTO snippets (title, content, created, expires)
VALUES ('First autumn morning',
        'First autumn morning\nthe mirror I stare into\nshows my father''s face.\n\n– Murakami Kijo',
        now(),
        now());
CREATE TABLE users (
                       id serial primary key ,
                       name VARCHAR(255) NOT NULL,
                       email VARCHAR(255) NOT NULL,
                       hashed_password CHAR(60) NOT NULL,
                       created timestamp NOT NULL,
                       active BOOLEAN NOT NULL DEFAULT TRUE
);
ALTER TABLE users ADD CONSTRAINT users_uc_email UNIQUE (email);